# How to update Strapi Documentation Plugin with GitKraken

1. Clone this repo
2. Click add remote, add the official GitHub strapi repo as upstream
3. Right click upstream, click fetch upstream
4. Click pull
5. Right click master branch, click merge upstream into master
6. Fix any merge conflicts
7. Push to origin
8. Open packages/plugins/documentation in terminal
9. Run `npm pack`
10. Create a new release on GitHub, upload the generated .tgz as an attachment
11. Copy the link to the .tgz and replace in package.json
